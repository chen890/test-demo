<?php

namespace App\Http\Controllers;
use App\User;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $id=Auth::id();
       // $task=User::find($id)->tasks;
        $task=Task::all();
        return view('tasks.index', ['tasks'=> $task]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tasks.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task=new Task();
        $id=Auth::id();
        $task->title = $request->title;
        $task->user_id=$id; 
        $task->status=0;            // תיקון שגיאת הסטטוס בהוספה
        $task->save();
        return redirect('tasks');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::find($id);
        return view('tasks.edit' , compact('task'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::find($id);
        $task -> update($request->except(['_token']));
        $task->update();
        return redirect('tasks');
    }
  
    public function statusUpdate(Request $request, $id)
    {
        if (!Gate::allows('admin'))
        {
            abort(403,"You are not allowed to mark tasks as dome..");
         }          
        $task = Task::findOrFail($id);            
        $task->status = 1; 
        $task -> update($request->except(['_token']));
        $task->update();
        return redirect('tasks');  
    }
    public function personal($id)
    {
        
        $id=Auth::id();
        $task=User::find($id)->tasks;
        return view('tasks.index', ['tasks'=> $task]);
 
        // //only if this todo belongs to user 
        // if (Gate::allows('admin')) {
        //     abort(403,"You are not allowed to mark tasks as dome..");
        //  }
        //  if (Gate::allows('user')) {
        //     abort(403,"You are not allowed to mark tasks as dome..");
        //  }    
        // $id=Auth::id();
        // $task = Task::findOrFail($id);            
        // return redirect('tasks');    
    } 

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::allows('admin'))
        {
            $task = Task::findOrFail($id);
            $task->delete();
            return redirect('tasks');
        }
    }
}
