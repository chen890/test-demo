<?php

namespace App;
use App\Task;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'title'
    ];
    public function users()
    {
        return $this->belongsTo('App\User');
    }

}

