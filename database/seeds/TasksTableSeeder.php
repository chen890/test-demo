<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert(
            [
                [
                    'title' => 'task test 1',
                    'status' => '0',
                    'user_id' => '1',
                                      
                ],
                [
                    'title' => 'task test 2',
                    'status' => '0',
                    'user_id' => '1',
                ],
                [
                    'title' => 'task test 3',
                    'status' => '0',
                    'user_id' => '1',
                ],                
            ]);
    }
}
