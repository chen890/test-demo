
@extends('layouts.app')
@section('content')




<h1>All Tasks List:</h1>

    <ul>
        @foreach($tasks as $task)
        <li>
        @if ($task->status == 0)      
            @can('admin')
            <a href="{{route('statusUpdate', $task->id)}}"> Mark As done </a>
            @endcan
            @else
              Done!
        @endif
        id: {{$task->id}} 
        {{$task->title}}
        <a href="{{route('tasks.edit' , $task->id)}}">  Edit    </a>
         @can('admin')
            @method('Delete') 
            <!-- <form action="{{action('TaskController@destroy', $task->id)}}" method='post'> -->

            <a href="{{route('delete', $task->id)}}">   Delete </a> 
        @endcan
        </li>
        @endforeach
    </ul>


    <a href="{{route('tasks.create')}}"> Create New Task</a>
    
    <script>
       $(document).ready(function()
       {
           $(":checkbox").click(function(event)
           {
               $.ajax(
                {
                    url: "{{url('tasks')}}" + '/' + event.target.id,
                    dataType:'json',
                    type:'put' ,
                    contentType:'application/json',
                    data:JSON.stringify({'status':event.target.checked,_token:'{{csrf_token()}}'}),
                    processData:false,
                    success:function(data)
                    {
                        console.log(JSON.stringify(data));
                    },
                    error:function(errorThrown)
                    {
                        console.log(errorThrown);
                    }
               });               
           });
       });
   </script>

@endsection